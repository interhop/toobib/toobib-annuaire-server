/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

DROP TABLE IF EXISTS provider;
DROP TABLE IF EXISTS attachment;

CREATE TABLE provider (
	provider_id					INTEGER		NOT NULL,
	username					VARCHAR(255)	NULL,
	password					VARCHAR(255)	NULL,
	email						VARCHAR(255)	NULL,
	title						VARCHAR(255)	NULL,
	firstname					VARCHAR(255)	NULL,
	lastname					VARCHAR(255)	NULL,
	rpps						VARCHAR(255)	NULL,
--	cps						BYTEA		NULL,
	date_of_birth					TIMESTAMP	NULL,
	gender						VARCHAR(50)	NULL,
	npi						VARCHAR(20)	NULL,
	is_admin					INTEGER		NULL,
	has_photo					INTEGER		NULL,
	first_connexion_datetime			TIMESTAMP	NULL,
	last_connexion_datetime				TIMESTAMP	NULL,
	last_connexion_failure				TIMESTAMP	NULL,
	valid_start_datetime				TIMESTAMP	NULL,
	valid_end_datetime				TIMESTAMP	NULL,
	invalid_reason					VARCHAR(1)	NULL,
	insert_datetime					TIMESTAMP	NULL,
	update_datetime					TIMESTAMP	NULL,
	delete_datetime					TIMESTAMP	NULL
);

CREATE TABLE attachment (
	attachment_id					INTEGER 	NOT NULL,
	provider_id					INTEGER		NOT NULL,
	attachment_type_value				VARCHAR(255)	NULL,
	attachment_type_concept_id			VARCHAR(255)	NULL,
	data						VARCHAR(255)	NULL,
	hash						VARCHAR(255)	NULL,
	size						INTEGER		NULL,
	url						VARCHAR(255)	NULL,
	language					VARCHAR(255)	NULL,
	title						VARCHAR(255)	NULL,
	rank						INTEGER		NULL,
	valid_start_datetime				TIMESTAMP	NULL,
	valid_end_datetime				TIMESTAMP	NULL,
	invalid_reason					TIMESTAMP	NULL,
	delete_datetime					TIMESTAMP	NULL,
	insert_datetime					TIMESTAMP 	NULL,
	update_datetime					TIMESTAMP	NULL
);

