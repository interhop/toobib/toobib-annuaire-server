/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

DROP index
TRUNCATE TABLE

*************************/

DROP INDEX IF EXISTS idx_concept_concept_code;
DROP INDEX IF EXISTS idx_concept_vocabulary_id;
DROP INDEX IF EXISTS idx_concept_domain_id;

DROP INDEX IF EXISTS idx_recherche_nom_normalise;
DROP INDEX IF EXISTS idx_recherche_prenom_normalise;
DROP INDEX IF EXISTS idx_recherche_commune_normalise;
DROP INDEX IF EXISTS idx_recherche_profession_normalise;
DROP INDEX IF EXISTS idx_recherche_savoir_faire_normalise;
DROP INDEX IF EXISTS idx_recherche_commune_normalise;
DROP INDEX IF EXISTS idx_recherche_rpps;
DROP INDEX IF EXISTS idx_recherche_model;

DROP INDEX IF EXISTS idx_professionnel_rpps; 
DROP INDEX IF EXISTS idx_etablissement_rpps;
DROP INDEX IF EXISTS idx_cps_rpps;
DROP INDEX IF EXISTS idx_mss_rpps;

TRUNCATE TABLE professionnel CASCADE;
TRUNCATE TABLE etablissement CASCADE;
TRUNCATE TABLE cps CASCADE;
TRUNCATE TABLE mss CASCADE;
DROP MATERIALIZED VIEW IF EXISTS recherche;

/************************

FILL API TABLES

*************************/

-- profession
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
        , t_language_id
)
SELECT DISTINCT p.libelle_profession 				
	, p.code_profession 					
	, 'PROFESSIONNEL'
	, 'PROFESSION'
	, 'FR'
FROM ps_libreacces_personne_activite p
WHERE p.libelle_profession IS NOT NULL
;

--- categorie_professionnelle
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
        , t_language_id
)
SELECT DISTINCT  p.libelle_categorie_professionnelle 		
	, p.code_categorie_professionnelle 			
	, 'PROFESSIONNEL'
	, 'CATEGORIE_PROFESSIONNELLE'
	, 'FR'
FROM ps_libreacces_personne_activite p
WHERE p.libelle_categorie_professionnelle IS NOT NULL
;

--- savoir_faire
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
        , t_language_id
)
SELECT DISTINCT p.libelle_savoirfaire 		
	, p.code_savoirfaire 			
	, 'PROFESSIONNEL'
	, 'SAVOIR_FAIRE'
	, 'FR'
FROM ps_libreacces_personne_activite p
WHERE p.libelle_savoirfaire IS NOT NULL
;

CREATE INDEX idx_concept_concept_code ON concept (concept_code ASC);
CREATE INDEX idx_concept_vocabulary_id ON concept (vocabulary_id ASC);
CREATE INDEX idx_concept_domain_id ON concept (domain_id ASC);




-- professionnel
INSERT INTO professionnel (
        rpps                                            
        , civilite                                        
        , civilite_exercice                               
        , nom                                             
        , nom_normalise                                   
        , prenom                                          
        , prenom_normalise                                
        , profession_id                                   
        , profession                                      
        , categorie_professionnelle_id                    
        , categorie_professionnelle            
        , savoir_faire_id                                 
        , savoir_faire                                    
)
SELECT distinct p.identification_nationale_pp 			
	, p.libelle_civilite 					
	, p.libelle_civilite_exercice 			
	, p.nom 						
	, multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb)			
	, p.prenom 						
	, multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb)	
	, prof.concept_id
	, p.libelle_profession	
	, cp.concept_id
	, p.libelle_categorie_professionnelle
	, sf.concept_id
	, p.libelle_savoirfaire 				
FROM ps_libreacces_personne_activite p
	LEFT JOIN concept prof ON (p.libelle_profession = prof.concept_name) AND prof.vocabulary_id = 'PROFESSION'
	LEFT JOIN concept cp ON (p.libelle_categorie_professionnelle = cp.concept_name) AND cp.vocabulary_id = 'CATEGORIE_PROFESSIONNELLE'
	LEFT JOIN concept sf ON (p.libelle_savoirfaire = sf.concept_name) AND sf.vocabulary_id = 'SAVOIR_FAIRE'
;


-- etablissement
INSERT INTO etablissement (
        rpps                                            
        , mode_exercice                                   
        , siret                                           
        , siren                                           
        , finess                                          
        , finess_etablissement_juridique                  
        , numero_voie                                     
        , indice_repetition_voie                          
        , type_de_voie                                    
        , voie                                            
        , mention_distribution                            
        , bureau_cedex                                    
        , code_postal                                     
        , code_commune                                    
        , commune                                 
        , commune_normalise                       
        , telephone                                       
        , telephone_2                                     
        , telecopie                                       
        , email                                           
        , gps_finess                                      
        , gps_code_commune                                
        , gps_code_postal                                
 
)
SELECT DISTINCT p.identification_nationale_pp
	,  p.libelle_mode_exercice
	,  p.siret
	,  p.siren
	,  p.finess
	,  p.finess_etablissement_juridique
	,  p.numero_voie
	,  p.indice_repetition_voie
	,  p.libelle_type_de_voie
	,  p.libelle_voie
	,  p.mention_distribution
	,  p.bureau_cedex
	,  p.code_postal
	,  p.code_commune
	,  p.libelle_commune
	,  multi_replace(lower(public.unaccent(p.libelle_commune)), '{" ": "", "-":""}'::jsonb) 	
	,  p.telephone
	,  p.telephone_2
	,  p.telecopie
	,  p.email
	, fg.coord_x || ', ' || fg.coord_y
        , poste_cm.gps                                
        , poste_cp.gps                                
FROM ps_libreacces_personne_activite p
LEFT JOIN finess_geo fg ON (p.finess = fg.finess_et) 				-- finess_et is unique in finess_geo table
LEFT JOIN laposte_commune poste_cm ON (p.code_commune = poste_cm.code_commune_insee)
LEFT JOIN laposte_postal poste_cp ON (p.code_postal = poste_cp.code_postal)

;

-- cps
INSERT INTO cps (
        rpps                                            
        , type_carte                                      
        , numero_carte                                    
        , id_national_carte                               
        , debut_valid_date                                
        , fin_valid_date                                  
        , opposition_date                                 
        , miseajour_date                                  
)
SELECT DISTINCT identification_nationale_pp
	, code_type_carte
	, numero_carte
	, id_national_carte
	, debut_valid_date
	, fin_valid_date
	, opposition_date
	, miseajour_date
FROM porteurs_cps_cpf
; 



-- mss
INSERT INTO mss (
        rpps     
        , type_bal 
        , bal      
)
SELECT DISTINCT identification_nationale_pp
    , type_bal
    , addresse_bal
FROM extraction_correspondance_mssante
;



--- recherche
--- CREATE MATERIAZED VIEWS AS avec index
CREATE MATERIALIZED VIEW recherche AS
	SELECT  p.identification_nationale_pp 											AS rpps
		, p.nom														AS nom
		, coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), '')			AS nom_normalise
		, p.prenom													AS prenom
		, coalesce(multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb), '')			AS prenom_normalise
		, p.libelle_profession												AS profession
		, coalesce(multi_replace(lower(public.unaccent(p.libelle_profession)), '{" ": "", "-":""}'::jsonb), '')		AS profession_normalise
		, p.libelle_savoirfaire												AS savoir_faire
		, coalesce(multi_replace(lower(public.unaccent(p.libelle_savoirfaire)), '{" ": "", "-":""}'::jsonb), '')	AS savoir_faire_normalise
		, p.libelle_commune												AS commune
		, coalesce(multi_replace(lower(public.unaccent(p.libelle_commune)), '{" ": "", "-":""}'::jsonb), '')		AS commune_normalise
		, 'EXTERNAL'													AS model
	FROM ps_libreacces_personne_activite p
;



/************************

Create Index

*************************/

CREATE INDEX idx_recherche_rpps ON recherche  (rpps ASC);
CREATE INDEX idx_recherche_prenom_normalise ON recherche USING GIN (prenom_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_nom_normalise ON recherche USING GIN (nom_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_profession_normalise ON recherche  USING GIN (profession_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_savoir_faire_normalise ON recherche USING GIN (savoir_faire_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_commune_normalise ON recherche USING GIN (commune_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_model ON recherche  (model ASC);
-- CREATE INDEX idx_recherche_profession ON recherche  (libelle_profession ASC);

CREATE INDEX idx_professionnel_rpps ON professionnel  (rpps ASC);
CREATE INDEX idx_etablissement_rpps ON etablissement  (rpps ASC);
CREATE INDEX idx_cps_rpps ON cps  (rpps ASC);
CREATE INDEX idx_mss_rpps ON mss  (rpps ASC);
