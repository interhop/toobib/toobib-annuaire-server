/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

Profesionnels de sante Libre Access

************************/

TRUNCATE TABLE ps_libreacces_personne_activite CASCADE;
DROP INDEX IF EXISTS idx_personne_activite_id_nat_pp;
DROP INDEX IF EXISTS idx_personne_activite_profession;
DROP INDEX IF EXISTS idx_personne_activite_categorie_professionnelle;
DROP INDEX IF EXISTS idx_personne_activite_savoirfaire;
DROP INDEX IF EXISTS idx_personne_activite_finess;
DROP INDEX IF EXISTS idx_personne_activite_code_commune;
DROP INDEX IF EXISTS idx_personne_activite_code_postal;

TRUNCATE TABLE ps_libreacces_dipl_autexerc CASCADE;
DROP INDEX IF EXISTS idx_dipl_autexerc_id_nat_pp;

TRUNCATE TABLE ps_libreacces_savoirfaire CASCADE;
DROP INDEX IF EXISTS idx_dipl_savoirfaire_id_nat_pp;

\copy ps_libreacces_personne_activite FROM 'download/PS_LibreAcces_Personne_activite.csv' WITH DELIMITER '|' CSV HEADER ;
\copy ps_libreacces_dipl_autexerc FROM 'download/PS_LibreAcces_Dipl_AutExerc.csv' WITH DELIMITER '|' CSV HEADER ;
\copy ps_libreacces_savoirfaire FROM 'download/PS_LibreAcces_SavoirFaire.csv' WITH DELIMITER '|' CSV HEADER ;

/************************

Carte CPS

************************/

TRUNCATE TABLE porteurs_CPS_CPF CASCADE;
DROP INDEX IF EXISTS idx_porteurs_CPS_CPF_id_nat_pp;

\copy porteurs_CPS_CPF FROM 'download/Porteurs_CPS_CPF.csv' WITH DELIMITER '|' CSV HEADER ;

/************************

Boite aux lettres (BAT) MSSante

************************/

TRUNCATE TABLE extraction_correspondance_mssante CASCADE;
DROP INDEX IF EXISTS idx_mss_id_nat_pp;

\copy extraction_correspondance_mssante FROM 'download/Extraction_Correspondance_MSSante.csv' WITH DELIMITER '|' CSV HEADER ;

/************************

INSEE Laposte

************************/

TRUNCATE TABLE laposte CASCADE;
DROP INDEX IF EXISTS idx_laposte_code_commune;
DROP INDEX IF EXISTS idx_laposte_code_postal;

\copy laposte FROM 'download/laposte.csv' WITH DELIMITER '|' CSV HEADER ;


DROP INDEX IF EXISTS idx_laposte_commune_code_commune;
INSERT INTO laposte_commune (
	code_commune_insee
	, gps

)
WITH tmp AS (
	SELECT *,
	row_number() over (partition by code_commune_insee order by code_commune_insee asc) as rn
	from laposte
)
SELECT code_commune_insee, gps
FROM tmp
where rn = 1
;

DROP INDEX IF EXISTS idx_laposte_postal_code_postal;
INSERT INTO laposte_postal (
	code_postal
	, gps
)
WITH tmp AS (
	SELECT *,
	row_number() over (partition by code_postal order by code_postal asc) as rn
	from laposte
)
SELECT code_postal, gps
FROM tmp
where rn = 1
;


/************************

FINESS

************************/

TRUNCATE TABLE finess_struct CASCADE;
DROP INDEX IF EXISTS idx_finess_struct_finess_et;

\copy finess_struct FROM 'download/finess_struct.csv' WITH DELIMITER ';' CSV ENCODING 'latin1';


TRUNCATE TABLE finess_geo CASCADE;
DROP INDEX IF EXISTS idx_finess_geo_finess_et;

\copy finess_geo FROM 'download/finess_geo.csv' WITH DELIMITER ';' CSV ENCODING 'latin1';

/************************

Create Index

************************/

CREATE INDEX idx_personne_activite_id_nat_pp  ON  ps_libreacces_personne_activite (identification_nationale_PP ASC);
CLUSTER ps_libreacces_personne_activite  USING idx_personne_activite_id_nat_pp ;
-- CREATE INDEX idx_personne_activite_code_commune;
CREATE INDEX idx_personne_activite_finess  ON  ps_libreacces_personne_activite (finess ASC);
CREATE INDEX idx_personne_activite_code_commune  ON  ps_libreacces_personne_activite (code_commune ASC);
CREATE INDEX idx_personne_activite_code_postal  ON  ps_libreacces_personne_activite (code_postal ASC);
CREATE INDEX idx_personne_activite_profession  ON  ps_libreacces_personne_activite (libelle_profession ASC);
CREATE INDEX idx_personne_activite_categorie_professionnelle  ON  ps_libreacces_personne_activite (libelle_categorie_professionnelle ASC);
CREATE INDEX idx_personne_activite_savoirfaire  ON  ps_libreacces_personne_activite (libelle_savoirfaire ASC);

CREATE INDEX idx_dipl_autexerc_id_nat_pp  ON   ps_libreacces_dipl_autexerc (identification_nationale_PP ASC);
CLUSTER ps_libreacces_dipl_autexerc USING idx_dipl_autexerc_id_nat_pp;

CREATE INDEX idx_dipl_savoirfaire_id_nat_pp  ON ps_libreacces_savoirfaire (identification_nationale_PP ASC);
CLUSTER ps_libreacces_savoirfaire USING idx_dipl_savoirfaire_id_nat_pp;

CREATE INDEX idx_porteurs_CPS_CPF_id_nat_pp  ON  porteurs_CPS_CPF (identification_nationale_PP ASC);
CLUSTER porteurs_CPS_CPF USING idx_porteurs_CPS_CPF_id_nat_pp;

CREATE INDEX idx_mss_id_nat_pp ON  extraction_correspondance_mssante (identification_nationale_PP ASC);
CLUSTER extraction_correspondance_mssante USING idx_mss_id_nat_pp;

CREATE INDEX idx_laposte_code_commune  ON  laposte (code_commune_insee ASC);
CLUSTER laposte USING idx_laposte_code_commune;

CREATE INDEX  idx_laposte_postal_code_postal ON laposte_postal (code_postal ASC);
CLUSTER laposte_postal USING  idx_laposte_postal_code_postal;

CREATE INDEX  idx_laposte_commune_code_commune ON laposte_commune (code_commune_insee ASC);
CLUSTER laposte_commune USING  idx_laposte_commune_code_commune;

CREATE INDEX idx_finess_struct_finess_et  ON  finess_struct (finess_et ASC);
CLUSTER finess_struct USING idx_finess_struct_finess_et;

CREATE INDEX idx_finess_geo_finess_et  ON  finess_geo (finess_et ASC);
CLUSTER finess_geo USING idx_finess_geo_finess_et;
