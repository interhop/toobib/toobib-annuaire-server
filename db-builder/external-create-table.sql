/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ###### 
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ###### 
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ###### 

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

Profesionnels de sante Libre Access

 ************************/

DROP TABLE IF EXISTS ps_libreacces_personne_activite;
DROP TABLE IF EXISTS ps_libreacces_dipl_autexerc;
DROP TABLE IF EXISTS ps_libreacces_savoirfaire;

CREATE TABLE ps_libreacces_personne_activite (
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	code_civilite_exercice 				TEXT	NULL,
	libelle_civilite_exercice 			TEXT	NULL,
	code_civilite 					TEXT	NULL,
	libelle_civilite 				TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_categorie_professionnelle 			TEXT	NULL,
	libelle_categorie_professionnelle 		TEXT	NULL,
	code_type_savoirfaire 				TEXT	NULL,
	libelle_type_savoirfaire  			TEXT	NULL,
	code_savoirfaire 				TEXT	NULL,
	libelle_savoirfaire  				TEXT	NULL,
	code_mode_exercice 				TEXT	NULL,
	libelle_mode_exercice 				TEXT	NULL,
	siret 						TEXT	NULL,
	siren						TEXT	NULL,
	finess 						TEXT	NULL,
	finess_etablissement_juridique 			TEXT	NULL,
	id_technique_structure 				TEXT	NULL,
	raison_sociale 					TEXT	NULL,
	enseigne_com  					TEXT	NULL,
	complement_destinataire  			TEXT	NULL,
	complement_point_geographique  			TEXT	NULL,
	numero_voie           				TEXT	NULL,
	indice_repetition_voie    			TEXT	NULL,
	code_type_de_voie 				TEXT	NULL,
	libelle_type_de_voie				TEXT	NULL,
	libelle_voie					TEXT	NULL,
	mention_distribution				TEXT	NULL,
	bureau_cedex					TEXT	NULL,
	code_postal					TEXT	NULL,
	code_commune					TEXT	NULL,
	libelle_commune					TEXT	NULL,
	code_pays					TEXT	NULL,
	libelle_pays					TEXT	NULL,
	telephone					TEXT	NULL,
	telephone_2					TEXT	NULL,
	telecopie					TEXT	NULL,
	email						TEXT	NULL,
	code_departement				TEXT	NULL,
	libelle_departement				TEXT	NULL,
	ancien_id_structure				TEXT	NULL,
	autorite_enregistrement				TEXT	NULL,
	code_secteur_activite				TEXT	NULL,
	libelle_secteur_activite			TEXT	NULL,
	code_section_tableau_pharmaciens		TEXT	NULL,
	libelle_section_tableau_pharmaciens		TEXT	NULL
)
;


CREATE TABLE ps_libreacces_dipl_autexerc (
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_type_diplome 				TEXT	NULL,
	libelle_type_diplome  				TEXT	NULL,
	code_diplome 					TEXT	NULL,
	libelle_diplome  				TEXT	NULL,
	code_type_autorisation 				TEXT	NULL,
	libelle_type_autorisation 			TEXT	NULL,
	code_discipline_autorisation 			TEXT	NULL,
	libelle_discipline_autorisation 		TEXT	NULL
)
;

CREATE TABLE ps_libreacces_savoirfaire (
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_categorie_professionnelle 			TEXT	NULL,
	libelle_categorie_professionnelle 		TEXT	NULL,
	code_type_savoirfaire 				TEXT	NULL,
	libelle_type_savoirfaire  			TEXT	NULL,
	code_savoirfaire 				TEXT	NULL,
	libelle_savoirfaire  				TEXT	NULL
)
;

/************************

Carte CPS

 ************************/
DROP TABLE IF EXISTS porteurs_CPS_CPF;


CREATE TABLE porteurs_CPS_CPF (
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	type_identifiant_PP 				TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_categorie_professionnelle 			TEXT	NULL,
	libelle_categorie_professionnelle 		TEXT	NULL,
	code_type_carte 				TEXT	NULL,
	libelle_carte   				TEXT	NULL,
	numero_carte 	  				TEXT	NULL,
	id_national_carte  				TEXT	NULL,
	debut_valid_date   				TEXT	NULL,
	fin_valid_date   				TEXT	NULL,
	opposition_date   				TEXT	NULL,
	miseajour_date   				TEXT	NULL
)
;

/************************

Boite aux lettres (BAT) MSSante

 ************************/

DROP TABLE IF EXISTS extraction_correspondance_mssante;


CREATE TABLE extraction_correspondance_mssante (
	type_bal 					TEXT	NULL,
	addresse_bal 					TEXT	NULL,
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	type_id_structure 				TEXT	NULL,
	id_structure 					TEXT	NULL,
	service_rattachement 				TEXT	NULL,
	civilite 					TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	cat_profession 					TEXT	NULL,
	libelle_cat_profession 				TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_savoirfaire 				TEXT	NULL,
	libelle_savoirfaire  				TEXT	NULL,
	dematerialisation  				TEXT	NULL,
	raison_sociale  				TEXT	NULL,
	enseigne_com  					TEXT	NULL,
	L2COMPLEMENTLOCALISATION_structure              TEXT	NULL,
	L3COMPLEMENTDISTRIBUTION_structure              TEXT	NULL,
	L4NUMEROVOIE_structure                          TEXT	NULL,
	L4COMPLEMENTNUMEROVOIE_structure                TEXT	NULL,
	NL4TYPEVOIE_structure                           TEXT	NULL,
	L4LIBELLEVOIE_structure                         TEXT	NULL,
	L5LIEUDITMENTION_structure                      TEXT	NULL,
	L6LIGNEACHEMINEMENT_structure                   TEXT	NULL,
	code_postal_structure                           TEXT	NULL,
	departement_structure                           TEXT	NULL,
	pays_structure 					TEXT 	NULL
)
;

/************************

FINESS Extraction du Fichier des établissements 

Liste des établissements du domaine sanitaire et social.
Informations sur la Géo-localisation : Le système d’information source contenant les coordonnées géographiques permettant de géo-localiser les établissements

https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/#_

 ************************/

DROP TABLE IF EXISTS finess_struct;

CREATE TABLE finess_struct (
	finess_et					TEXT		NULL,
	finess_ej                                       TEXT		NULL,
	raison_sociale                                  TEXT		NULL,
	raison_sociale_longue                           TEXT		NULL,
	complement_raison_sociale                       TEXT		NULL,
	complement_distribution                         TEXT		NULL,
	numero_voie					TEXT		NULL,
	type_voie                                       TEXT		NULL,
	libelle_voie                                    TEXT		NULL,
	complement_voie                                 TEXT		NULL,
	lieu_dit_bp                                     TEXT		NULL,
	code_ommune                                     TEXT		NULL,
	departement					TEXT		NULL,
	libelle_departement                             TEXT		NULL,
	ligne_acheminement                              TEXT		NULL,
	telephone                                       TEXT		NULL,
	telecopie                                       TEXT		NULL,
	Categorie_etablissement                         TEXT		NULL,
	libelle_categorie_tablissement                  TEXT		NULL,
	categorie_agregat_etablissement                 TEXT		NULL,
	libelle_categorie_agregat_etablissement         TEXT		NULL,
	siret                                           TEXT		NULL,
	code_ape                                        TEXT		NULL,
	code_mft                                        TEXT		NULL,
	libelle_mft					TEXT		NULL,			
	code_sph                                        TEXT		NULL,
	libelle_SPH                                     TEXT		NULL,
	date_ouverture                                  TEXT		NULL,
	date_autorisation                               TEXT		NULL,
	date_maj_structure                              TEXT		NULL,
	numero_education_national                       TEXT		NULL
)                                                       
;                                                       
                                                        
DROP TABLE IF EXISTS finess_geo;                        
                                                        
CREATE TABLE finess_geo (
	finess_et					TEXT		NULL,
	coord_x						TEXT		NULL,
	coord_y                                         TEXT		NULL,
	source_coord                                    TEXT		NULL,
	date_maj                                        TEXT		NULL
) 
;

/************************

La base officielle des codes postaux est un jeu de données qui fournit la 
correspondance entre les codes postaux et les codes INSEE des communes, 
de France (métropole et DOM), des TOM, ainsi que de MONACO. 


https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information/?disjunctive.code_commune_insee&disjunctive.nom_de_la_commune&disjunctive.code_postal&disjunctive.ligne_5

 ************************/

DROP TABLE IF EXISTS laposte;

CREATE TABLE laposte (
	code_commune_insee 		 		TEXT		NULL,
	nom_commune 					TEXT		NULL,
	code_postal 				 	TEXT		NULL,
	ligne_5 					TEXT		NULL,
	libelle_acheminement 				TEXT		NULL,
	gps		 				TEXT		NULL
)
;

DROP TABLE IF EXISTS laposte_postal;

CREATE TABLE laposte_postal (
	code_postal 				 	TEXT		NULL,
	gps		 				TEXT		NULL
)
;

DROP TABLE IF EXISTS laposte_commune;

CREATE TABLE laposte_commune (
	code_commune_insee 				TEXT		NULL,
	gps		 				TEXT		NULL
)
;

/************************

Derived tables from csv

 ************************/

-- DROP TABLE IF EXISTS profession;
-- DROP TABLE IF EXISTS categorie_professionnelle;
-- DROP TABLE IF EXISTS savoir_faire;
-- DROP TABLE IF EXISTS recherche;
DROP TABLE IF EXISTS concept;


-- CREATE TABLE profession (
-- 	profession_id 					INTEGER 	NOT NULL,
-- 	code_profession 				TEXT		NULL,
-- 	profession 					TEXT		NULL,
-- 	profession_normalise 				TEXT		NULL,
-- 	insert_datetime					TIMESTAMP 	NOT NULL
-- );
-- 
-- CREATE TABLE savoir_faire (
-- 	savoir_faire_id 				INTEGER 	NOT NULL,
-- 	code_savoir_faire				TEXT		NULL,
-- 	savoir_faire 					TEXT		NULL,
-- 	savoir_faire_normalise				TEXT		NULL,
-- 	insert_datetime					TIMESTAMP 	NOT NULL
-- );
-- 
-- 
-- CREATE TABLE categorie_professionnelle (
-- 	categorie_professionnelle_id			INTEGER		NOT NULL,
-- 	code_categorie_professionnelle 			TEXT		NULL,
-- 	categorie_professionnelle 			TEXT		NULL,
-- 	categorie_professionnelle_normalise		TEXT		NULL,
-- 	insert_datetime					TIMESTAMP 	NOT NULL
-- );
-- 
-- CREATE TABLE recherche (
-- 	rech_id 					INTEGER 	NOT NULL,
-- 	search 		 				TEXT		NULL,
-- 	rpps 				 		TEXT		NULL,
-- 	pp 			 			TEXT		NULL,
-- 	civilite 					TEXT		NULL,
-- 	civilite_exercice	 			TEXT		NULL,
-- 	nom 						TEXT		NULL,
-- 	nom_normalise 					TEXT		NULL,
-- 	prenom 						TEXT		NULL,
-- 	prenom_normalise 				TEXT		NULL,
-- 	profession_id					INTEGER		NULL,
-- 	profession	 				TEXT		NULL, -- redonde le profesion_id
-- 	categorie_professionnelle_id			INTEGER		NULL,
-- 	categorie_professionnelle	 		TEXT		NULL, -- civil, mili, etudiant -- redonde le categorie_professionnelle_id
-- 	savoir_faire_id		  			INTEGER		NULL,
-- 	savoir_faire 	 				TEXT		NULL, -- redonde le savoirfaire_id
-- 	mode_exercice 					TEXT		NULL,
-- 	siret 						TEXT		NULL,
-- 	siren						TEXT		NULL,
-- 	finess 						TEXT		NULL,
-- 	finess_etablissement_juridique 			TEXT		NULL,
-- 	numero_voie           				TEXT		NULL,
-- 	indice_repetition_voie    			TEXT		NULL,
-- 	type_de_voie					TEXT		NULL,
-- 	voie						TEXT		NULL,
-- 	mention_distribution				TEXT		NULL,
-- 	bureau_cedex					TEXT		NULL,
-- 	code_postal					TEXT		NULL,
-- 	code_commune					TEXT		NULL,
-- 	commune						TEXT		NULL,
-- 	commune_normalise				TEXT		NULL,
-- 	telephone					TEXT		NULL,
-- 	telephone_2					TEXT		NULL,
-- 	telecopie					TEXT		NULL,
-- 	email						TEXT		NULL,
-- 	gps_finess					TEXT		NULL,
-- 	gps_code_commune				TEXT		NULL,
-- 	gps_code_postal					TEXT		NULL,
-- 	insert_datetime					TIMESTAMP 	NOT NULL
-- )
-- ;


CREATE TABLE concept (
	  concept_id					INTEGER			NOT NULL ,
	  concept_name					VARCHAR(255)		NOT NULL ,
	  domain_id					VARCHAR(50)		NOT NULL ,
	  vocabulary_id					VARCHAR(50)		NOT NULL ,
	  concept_class_id				VARCHAR(50)		NULL ,
	  standard_concept				VARCHAR(1)		NULL ,
	  concept_code					VARCHAR(50)		NOT NULL ,
	  valid_start_date				DATE			NOT NULL ,
	  valid_end_date				DATE			NULL ,
	  invalid_reason				VARCHAR(1)		NULL , 
	  t_language_id 				VARCHAR(3)		NOT NULL,
	  insert_datetime				TIMESTAMP 		NOT NULL
)
;


CREATE TABLE professionnel (
	pro_id						INTEGER		NOT NULL,
	rpps 				 		TEXT		NULL,
	civilite 					TEXT		NULL,
	civilite_exercice	 			TEXT		NULL,
	nom 						TEXT		NULL,
	nom_normalise 					TEXT		NULL,
	prenom 						TEXT		NULL,
	prenom_normalise 				TEXT		NULL,
	profession_id					INTEGER		NULL,
	profession	 				TEXT		NULL, -- redonde le profesion_id
	categorie_professionnelle_id			INTEGER		NULL,
	categorie_professionnelle	 		TEXT		NULL, -- civil, mili, etudiant -- redonde le categorie_professionnelle_id
	savoir_faire_id		  			TEXT		NULL,
	savoir_faire 	 				TEXT		NULL, -- redonde le savoirfaire_id
	insert_datetime					TIMESTAMP 	NOT NULL
);

CREATE TABLE etablissement (
	etab_id						INTEGER		NOT NULL,
	rpps 				 		TEXT		NULL,
	mode_exercice 					TEXT		NULL,
	siret 						TEXT		NULL,
	siren						TEXT		NULL,
	finess 						TEXT		NULL,
	finess_etablissement_juridique 			TEXT		NULL,
	numero_voie           				TEXT		NULL,
	indice_repetition_voie    			TEXT		NULL,
	type_de_voie					TEXT		NULL,
	voie						TEXT		NULL,
	mention_distribution				TEXT		NULL,
	bureau_cedex					TEXT		NULL,
	code_postal					TEXT		NULL,
	code_commune					TEXT		NULL,
	commune						TEXT		NULL,
	commune_normalise				TEXT		NULL,
	telephone					TEXT		NULL,
	telephone_2					TEXT		NULL,
	telecopie					TEXT		NULL,
	email						TEXT		NULL,
	gps_finess					TEXT		NULL,
	gps_code_commune				TEXT		NULL,
	gps_code_postal					TEXT		NULL,
	insert_datetime					TIMESTAMP 	NOT NULL
);

CREATE TABLE cps (
	cps_id						INTEGER		NOT NULL,
	rpps 				 		TEXT		NULL,
	type_carte 					TEXT	NULL,
	numero_carte 	  				TEXT	NULL,
	id_national_carte  				TEXT	NULL,
	debut_valid_date   				TEXT	NULL,
	fin_valid_date   				TEXT	NULL,
	opposition_date   				TEXT	NULL,
	miseajour_date   				TEXT	NULL,
	insert_datetime					TIMESTAMP 	NOT NULL
);

CREATE TABLE mss (
	mss_id						INTEGER		NOT NULL,
	rpps 				 		TEXT		NULL,
	type_bal 					TEXT		NULL,
	bal 						TEXT		NULL,
	insert_datetime					TIMESTAMP 	NOT NULL
);
