/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

ALTER TABLE provider ADD CONSTRAINT xpk_provider_table PRIMARY KEY (provider_id);
CREATE SEQUENCE provider_id_seq OWNED BY provider.provider_id;
ALTER TABLE provider ALTER COLUMN provider_id SET DEFAULT nextval('provider_id_seq');

ALTER TABLE provider ALTER COLUMN insert_datetime SET DEFAULT now();
ALTER TABLE provider ALTER COLUMN update_datetime SET DEFAULT NULL;
