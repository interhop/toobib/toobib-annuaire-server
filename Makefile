USER=toobib
DB=toobib
EXTERNAL_SCHEMA=external
INTERNAL_SCHEMA=internal
PG_CONF=host=localhost dbname=$(DB) user=$(USER)
EXTERNAL_DB_ARGUMENTS=$(PG_CONF) options=--search_path=$(EXTERNAL_SCHEMA)
INTERNAL_DB_ARGUMENTS=$(PG_CONF) options=--search_path=$(INTERNAL_SCHEMA)

all: downloader correction-csv external-create-schema external-load-row external-load-api internal-create-schema

external-reload: external-create-schema external-load-row external-load-api

external-reload: external-create-schema external-load-row external-load-api internal-create-schema

downloader:
	echo "download csv ..."
	echo "from service.annuaire.sante.fr ..."
	rm -f download/*
	wget --no-check-certificate https://service.annuaire.sante.fr/annuaire-sante-webservices/V300/services/extraction/PS_LibreAcces -P download/
	wget --no-check-certificate  https://service.annuaire.sante.fr/annuaire-sante-webservices/V300/services/extraction/Porteurs_CPS_CPF -P download/
	wget --no-check-certificate  https://service.annuaire.sante.fr/annuaire-sante-webservices/V300/services/extraction/Extraction_Correspondance_MSSante -P download/
	mv download/PS_LibreAcces download/PS_LibreAcces.zip
	mv download/Porteurs_CPS_CPF download/Porteurs_CPS_CPF.zip
	mv download/Extraction_Correspondance_MSSante download/Extraction_Correspondance_MSSante.zip
	unzip download/PS_LibreAcces.zip -d download/
	unzip download/Porteurs_CPS_CPF.zip -d download/
	unzip download/Extraction_Correspondance_MSSante.zip -d download/
	echo "from laposte code postal ..."
	wget --no-check-certificate 'https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=|' -P download/
	mv download/index.html\?format=csv\&timezone=Europe%2FBerlin\&lang=fr\&use_labels_for_header=true\&csv_separator=\| download/laposte.csv
	echo "from finess file ..."
	wget --no-check-certificate 'https://www.data.gouv.fr/fr/datasets/r/16ee2cd3-b9fe-459e-8a57-46e03ba3adbd' -P download/

correction-csv:
	sh db-builder/correction.sh
	

external-create-schema:
	psql "$(PG_CONF)" -c 'DROP SCHEMA IF EXISTS "$(EXTERNAL_SCHEMA)" CASCADE;'
	psql "$(PG_CONF)" -c 'CREATE SCHEMA "$(EXTERNAL_SCHEMA)";'
	echo "$(EXTERNAL_DB_ARGUMENTS)"
	psql "$(EXTERNAL_DB_ARGUMENTS)" -f "db-builder/external-create-table.sql"
	psql "$(EXTERNAL_DB_ARGUMENTS)" -f "db-builder/external-constraints.sql"
	psql "$(EXTERNAL_DB_ARGUMENTS)" -f "db-builder/functions.sql"

external-load-row:
	echo "Load csv in db..."
	psql "$(EXTERNAL_DB_ARGUMENTS)" -f "db-builder/external-load-row-data.sql"

external-load-api:
	echo "Build api table..."
	psql "$(EXTERNAL_DB_ARGUMENTS)" -f "db-builder/external-load-api.sql"

internal-create-schema:
	psql "$(PG_CONF)" -c 'DROP SCHEMA IF EXISTS "$(INTERNAL_SCHEMA)" CASCADE;'
	psql "$(PG_CONF)" -c 'CREATE SCHEMA "$(INTERNAL_SCHEMA)";'
	echo "$(INTERNAL_DB_ARGUMENTS)"
	psql "$(INTERNAL_DB_ARGUMENTS)" -f "db-builder/internal-create-table.sql"
	psql "$(INTERNAL_DB_ARGUMENTS)" -f "db-builder/internal-constraints.sql"


