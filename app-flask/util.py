from unidecode import unidecode 

my_dict = {
    "libelleCommune" : "libelle_commune",
    "libelleProfession" : "libelle_profession",
}

def normalizeStrings(strings):
    list_of_strings = {}
    for key in strings:
        if strings[key]:
            list_of_strings[key] = unidecode(strings[key]).lower()
        else:
            list_of_strings[key] = ''
    return list_of_strings;

