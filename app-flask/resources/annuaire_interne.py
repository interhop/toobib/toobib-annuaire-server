from flask_restful import Resource, reqparse
import werkzeug 
from flask_jwt_extended import create_access_token, \
        get_jwt_claims, \
        get_jwt_identity, \
        jwt_required, \
        get_raw_jwt

from datetime import datetime, timedelta


from util import my_dict
from models.doctor import DoctorModel
from blacklist import BLACKLIST

class GetDoctor(Resource):

    @classmethod
    def get(cls, _id):
        doctor = DoctorModel.find_by_id(_id)

        if not doctor:
            return { 'message': 'Doctor not found'}, 404
        return doctor.json(), 200

class AddDoctor(Resource):
    
    parser = reqparse.RequestParser()
    parser.add_argument('lastname', type=str)
    parser.add_argument('firstname', type=str)
    parser.add_argument('username', type=str)
    parser.add_argument('password', type=str)
    parser.add_argument('email', type=str)
    parser.add_argument('rpps', type=str)
#    parser.add_argument('cps', type=werkzeug.datastructures.FileStorage, location='files')
    
    @classmethod
    def post(cls):
        args = cls.parser.parse_args()
        firstname = args['firstname']
        rpps = args['rpps']
        lastname = args['lastname']
        username = args['username']
        password = args['password']
        email = args['email']
 #       cps = args['cps']

        if DoctorModel.find_by_username(username):
            print(DoctorModel.find_by_username(username))
            return {'message': 'A user with that username already exists'}, 400
        if DoctorModel.find_by_email(email):
            return {'message': 'This email is in used by another user'}, 400
        
        doctor = DoctorModel(firstname, lastname, username, password, email, rpps)
        doctor.set_password(password)
        doctor.upserting()
        return doctor.json(), 202 # TODO: Check created code

# TODO:Change data of a doc

# TODO:DELETE data of a doc

class LoginDoctor(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('username', type=str, required=True, help="Vous devez renseigner un nom d'utilisateur")
    parser.add_argument('password', type=str, required=True, help="Vous devez renseigner un mot de passe")

    @classmethod
    def post(cls):
        data = cls.parser.parse_args()

        doctor = DoctorModel.find_by_username(data['username'])
        print(doctor.username)
        if doctor and doctor.check_password(data['password']) and doctor.invalid_reason != 'D': #TODO: method doctor_is_valid()
            expires = timedelta(days=1)
            access_token = create_access_token(identity=doctor, fresh=True, expires_delta=expires)
            return {
                    'access_token': access_token,
                    'doctor_id': doctor.id,
                    'username': doctor.username,
                    # 'is_admin': 'Admin' if doctor.is_admin == 1 else 'Not-Admin',
                    }, 200

        return {"message": "Invalid credentials"}, 401

class LogoutDoctor(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti'] # jti is "JWT ID", a unique identifier for a JWT.
        BLACKLIST.add(jti)
        return {"message": "Successfully logged out"}, 200
