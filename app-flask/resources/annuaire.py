from flask_restful import Resource, reqparse
from util import my_dict
from models.annuaire import AnnuaireModel


class Providers(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('search',  type=str, help="Provide an name (str)")

    parser.add_argument('nom',
            type=str,
            help="Provide an name (str)"
    )
    parser.add_argument('prenom',
            type=str,
            help="Provide a firstname (str)"
    )
    parser.add_argument('libelleProfession',
            type=str,
            help="Provide a job (str)"
    )
    parser.add_argument('libelleCommune',
            type=str,
            help="Provide a city (str)"
    )
    parser.add_argument('identificationNationalePP',
            type=str,
            help="Provide the identification number (str)"
    )
    parser.add_argument('page',
            type=int,
            help="Page to show (int)"
    )

    @classmethod
    def get(cls):
        data = cls.parser.parse_args()
        if data["page"] == None:
            page = 1
        elif data['page'] <= 0:
            return {'message': 'Page must be a positive integer'}, 404
        else:
            page = data["page"]
        limit = 12  # can't be moved
        providers = AnnuaireModel.find_providers(
            data,
            page=page,
            limit=limit
        )

        return {"has_next": providers.has_next, "has_prev": providers.has_prev,
                "next_num": providers.next_num, "prev_num": providers.prev_num,
                "total": providers.total, "pages": providers.pages,
                "providers":  [p.json() for p in providers.items]
            }, 200


class Provider(Resource):
    
    @classmethod
    def get(cls, _id):
        provider = AnnuaireModel.find_by_id(_id)
        if not provider:
            return {'message': 'Provider not found'}, 404
        return provider.json(), 200


class Distinct(Resource):
    parser = reqparse.RequestParser()

    parser.add_argument('search',
        type=str,
        help="Provide an name (str)"
    )

    @classmethod
    def get(cls, _column):
        data = cls.parser.parse_args()
        distinctValues = AnnuaireModel.find_distinct(
            column=_column, data=data)
        if not distinctValues:
            return {'values': []}, 200
        return {"values": [getattr(v, my_dict[_column]) for v in distinctValues]}, 200
