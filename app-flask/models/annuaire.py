from config import EXTERNAL_DIRECTORY_SCHEMA
from util import my_dict, normalizeStrings
from db import db
from sqlalchemy.orm import load_only

class AnnuaireModel(db.Model):
    __tablename__ = 'annuaire'
    __table_args__ = ({'schema': EXTERNAL_DIRECTORY_SCHEMA})

    id = db.Column('annuaire_id', db.Integer,
                   primary_key=True, autoincrement=True)

    search = db.Column(db.Text())  # Full Search Text in postgesql

    type_identifiant_pp = db.Column(db.Text())
    identifiant_pp = db.Column(db.Text())
    identification_nationale_pp = db.Column(db.Text())
    code_civilite_exercice = db.Column(db.Text())
    libelle_civilite_exercice = db.Column(db.Text())
    code_civilite = db.Column(db.Text())
    libelle_civilite = db.Column(db.Text())
    nom = db.Column(db.Text())
    nom_normalise = db.Column(db.Text()) #
    prenom = db.Column(db.Text())
    prenom_normalise = db.Column(db.Text()) #
    code_profession = db.Column(db.Text())
    libelle_profession = db.Column(db.Text()) #
    libelle_profession_normalise = db.Column(db.Text()) #
    code_categorie_professionnelle = db.Column(db.Text())
    libelle_categorie_professionnelle = db.Column(db.Text())
    code_type_savoirfaire = db.Column(db.Text())
    libelle_type_savoirfaire = db.Column(db.Text())
    code_savoirfaire = db.Column(db.Text())
    libelle_savoirfaire = db.Column(db.Text())
    code_mode_exercice = db.Column(db.Text())
    libelle_mode_exercice = db.Column(db.Text())
    siret = db.Column(db.Text())
    siren = db.Column(db.Text())
    finess = db.Column(db.Text())
    finess_etablissement_juridique = db.Column(db.Text())
    id_technique_structure = db.Column(db.Text())
    raison_sociale = db.Column(db.Text())
    enseigne_com = db.Column(db.Text())
    complement_destinataire = db.Column(db.Text())
    complement_point_geographique = db.Column(db.Text())
    numero_voie = db.Column(db.Text())
    indice_repetition_voie = db.Column(db.Text())
    code_type_de_voie = db.Column(db.Text())
    libelle_type_de_voie = db.Column(db.Text())
    libelle_voie = db.Column(db.Text())
    mention_distribution = db.Column(db.Text())
    bureau_cedex = db.Column(db.Text())
    code_postal = db.Column(db.Text())
    code_commune = db.Column(db.Text())
    libelle_commune = db.Column(db.Text())
    libelle_commune_normalise = db.Column(db.Text()) #
    code_pays = db.Column(db.Text())
    libelle_pays = db.Column(db.Text())
    telephone = db.Column(db.Text())
    telephone_2 = db.Column(db.Text())
    telecopie = db.Column(db.Text())
    email = db.Column(db.Text())
    code_departement = db.Column(db.Text())
    libelle_departement = db.Column(db.Text())
    ancien_id_structure = db.Column(db.Text())
    autorite_enregistrement = db.Column(db.Text())
    code_secteur_activite = db.Column(db.Text())
    libelle_secteur_activite = db.Column(db.Text())
    code_section_tableau_pharmaciens = db.Column(db.Text())
    libelle_section_tableau_pharmaciens = db.Column(db.Text())
    code_type_carte = db.Column(db.Text())
    libelle_carte = db.Column(db.Text())
    numero_carte = db.Column(db.Text())
    id_national_carte = db.Column(db.Text())
    debut_valid_date = db.Column(db.Text())
    fin_valid_date = db.Column(db.Text())
    opposition_date = db.Column(db.Text())
    miseajour_date = db.Column(db.Text())

    def __init__(self, firstname, lastname, job, city):
        self.prenom = firstname.title()
        self.nom = lastname.title()
        self.libelle_profession = job.lower()
        self.libelle_commune = city.lower()

    def json(self):
        return {
            'id': self.id,
            'identificationNationalePP': self.identification_nationale_pp,
            'prenom': self.prenom,
            'nom': self.nom,
            'telephone': self.telephone,
            'email': self.email,
            'libelleCommune': self.libelle_commune,
            'libelleProfession': self.libelle_profession,
            'codeDepartement': self.code_departement,
            'libelleDepartement': self.libelle_departement,
            'codePostal': self.code_postal,
            'bureauCedex': self.bureau_cedex,
            'codeCommune': self.code_commune,
            'enseigneCom': self.enseigne_com,
            'complementDestinataire': self.complement_destinataire,
            'complementPointGeographique': self.complement_point_geographique,
            'numeroVoie': self.numero_voie,
            'indiceRepetitionVoie': self.indice_repetition_voie,
            'codeTypeDeVoie': self.code_type_de_voie,
            'libelleTypeDeVoie': self.libelle_type_de_voie,
            'libelleVoie': self.libelle_voie,
        }

    @classmethod
    def find_by_id(cls, annuaire_id):
        return cls.query.filter_by(id=annuaire_id).first()

    @classmethod
    def find_providers(cls, data, page, limit):
        """
            Find providers in annuaire table
            Use GIN index of postgreSQL, cf READ.md
            If search parameter is filled others are ignored
        """
        query_set = None
        # pop = https://kite.com/python/answers/how-to-rename-a-dictionary-key-in-python
        # change javascript standard variable declaration to PEP
        data["libelle_profession_normalise"] = data.pop("libelleProfession")
        data["libelle_commune_normalise"] = data.pop("libelleCommune")
        data["nom_normalise"] = data.pop("nom")
        data["prenom_normalise"] = data.pop("prenom")
        data["identification_nationale_pp"] = data.pop("identificationNationalePP")
        if data['page']:
            del data['page']
        data = normalizeStrings(data)
        # print(data);
        if data['search']:  # for full search
            words = data['search'].split(' ')
            for w in words:
                if query_set:
                    query_set = query_set.filter(
                        cls.search.like('%' + w + '%'))
                else:
                    query_set = cls.query.filter(
                        cls.search.like('%' + w + '%'))

        if data['search']:
            del data['search']
        # for nom, prenom, libelle_commune, libelle_profession, identifiant_pp
        for k, v in data.items():
            if v and query_set:
                query_set = query_set.filter(getattr(cls, k).like('%' + v + '%'))
            elif v:
                query_set = cls.query.filter(getattr(cls, k).like('%' + v + '%'))

        # pagination
        if not query_set:
            query_set = cls.query
        return query_set.order_by(AnnuaireModel.nom).paginate(page=page,
                                                              per_page=limit, error_out=True, max_per_page=limit)

    @classmethod
    def find_distinct(cls, column, data):
        query_set = None
        if data['search']:  # for full search
            words = data['search'].split(' ')
            for w in words:
                if query_set:
                    query_set = query_set.filter(
                        cls.search.ilike('%' + w + '%'))
                else:
                    query_set = cls.query.filter(
                        cls.search.ilike('%' + w + '%'))
        else:
            query_set = cls.query
        # https://stackoverflow.com/questions/11530196/flask-sqlalchemy-query-specify-column-names
        return query_set.distinct(getattr(cls, my_dict[column])).all()
