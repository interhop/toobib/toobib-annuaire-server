from config import INTERNAL_DIRECTORY_SCHEMA
from db import db
from sqlalchemy.orm import load_only
from flask.json import JSONEncoder
from werkzeug.security import generate_password_hash, \
        check_password_hash
from datetime import datetime, timedelta

class DoctorModel(db.Model):
        __tablename__ = 'provider'
        __table_args__ = ({'schema': INTERNAL_DIRECTORY_SCHEMA})
        id = db.Column('provider_id', db.Integer,
                primary_key=True, autoincrement=True)
        username = db.Column(db.Text())
        password = db.Column(db.Text())
        email = db.Column(db.Text())
        rpps = db.Column(db.Text())
##       cps = db.Column(db.LargeBinary)
        title = db.Column(db.Text())
        firstname = db.Column(db.Text())
        lastname = db.Column(db.Text())
        date_of_birth = db.Column(db.Text())
        gender = db.Column(db.Text())
        npi = db.Column(db.Text())
        is_admin = db.Column(db.Text())
        has_photo = db.Column(db.Text())
        first_connexion_datetime = db.Column(db.DateTime())
        last_connexion_datetime = db.Column(db.DateTime())
        last_connexion_failure = db.Column(db.DateTime())
        valid_start_datetime = db.Column(db.DateTime(), default=datetime.now())
        valid_end_datetime = db.Column(db.DateTime(), default=datetime.now() + timedelta(weeks=52))
        invalid_reason = db.Column(db.Text())
        insert_datetime = db.Column(db.DateTime(), default=datetime.now())
        update_datetime = db.Column(db.Text())
        delete_datetime = db.Column(db.Text())

        def __init__(self, firstname, lastname, username, password, email, rpps):
                self.firstname = firstname.title()
                self.lastname = lastname.title()
                self.username = username
                self.password = password
                self.email = email
                self.rpps = rpps
               ## self.cps = cps

        def json(self):
                return {
                        'id': self.id,
                        'username': self.username,
                        'password': self.password,
                        'email': self.email,
                        'rpps': self.rpps,
                        'firstname': self.firstname,
                        'lastname': self.lastname,
                        'dateOfBirth': self.date_of_birth,
                        'gender': self.gender,
                        'npi': self.npi,
                        'isAdmin': self.is_admin,
                        'hasPhoto': self.has_photo,
                        'invalidReason': self.invalid_reason,
                }


        @classmethod
        def find_by_id(cls, annuaire_id):
            return cls.query.filter_by(id=annuaire_id).first()

        @classmethod
        def find_by_username(cls, username):
            return cls.query.filter_by(username=username).first()

        @classmethod
        def find_by_email(cls, email):
            return cls.query.filter_by(email=email).first()                
        
        def set_password(self, password):
            self.password = generate_password_hash(password, method='pbkdf2:sha256', salt_length=8)

        def check_password(self, password):
            return check_password_hash(self.password, password)
        
        def upserting(self):
            db.session.add(self)
            db.session.commit()
#
#
#    @classmethod
#    def find_providers(cls, data, page, limit):
#
#        """
#            Find providers in annuaire table
#            Use GIN index of postgreSQL, cf READ.md
#            If search parameter is filled others are ignored
#        """
#
#        query_set = None
#        # pop = https://kite.com/python/answers/how-to-rename-a-dictionary-key-in-python
#        # change javascript standard variable declaration to PEP
#        data["libelle_profession_normalise"] = data.pop("libelleProfession")
#        data["libelle_commune_normalise"] = data.pop("libelleCommune")
#        data["nom_normalise"] = data.pop("nom")
#        data["prenom_normalise"] = data.pop("prenom")
#        data["identification_nationale_pp"] = data.pop("identificationNationalePP")
#        if data['page']:
#            del data['page']
#        data = normalizeStrings(data)
#        # print(data);
#        if data['search']:  # for full search
#            words = data['search'].split(' ')
#            for w in words:
#                if query_set:
#                    query_set = query_set.filter(
#                        cls.search.like('%' + w + '%'))
#                else:
#                    query_set = cls.query.filter(
#                        cls.search.like('%' + w + '%'))
#
#        if data['search']:
#            del data['search']
#        # for nom, prenom, libelle_commune, libelle_profession, identifiant_pp
#        for k, v in data.items():
#            if v and query_set:
#                query_set = query_set.filter(getattr(cls, k).like('%' + v + '%'))
#            elif v:
#                query_set = cls.query.filter(getattr(cls, k).like('%' + v + '%'))
#
#        # pagination
#        if not query_set:
#            query_set = cls.query
#        return query_set.order_by(AnnuaireModel.nom).paginate(page=page,
#                                                              per_page=limit, error_out=True, max_per_page=limit)
#
