from flask import Flask
from flask_restful import Api
from flask_cors import CORS

from flask_jwt_extended import JWTManager

from resources.annuaire import Providers, Provider, Distinct
from resources.annuaire_interne import GetDoctor, AddDoctor, LoginDoctor, LogoutDoctor

from blacklist import BLACKLIST

# Create app and API
app = Flask(__name__)
app.config.from_object('config')
api = Api(app)

# Open cross url
cors = CORS(app, ressources={r"*": {"origins": "*"}})

# Link flask_jwt_extended to our app
jwt = JWTManager(app)

# Create a function that will be called whenever create_access_token
# is used. It will take whatever object is passed into the
# create_access_token method, and lets us define what custom claims
# should be added to the access token.
@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {
        'm_firstname': user.firstname,
        'm_lastname': user.lastname,
        'm_username': user.username,
        'm_email': user.email,
        'm_is_admin': user.is_admin
    }


# Create a function that will be called whenever create_access_token
# is used. It will take whatever object is passed into the
# create_access_token method, and lets us define what the identity
# of the access token should be.
@jwt.user_identity_loader
def user_identity_lookup(user):
        return user.id

# Allow disconnection
@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
            return decrypted_token['jti'] in BLACKLIST


# All the endpoints
api.add_resource(Providers, '/providers')
api.add_resource(Provider, '/provider/<int:_id>')
api.add_resource(Distinct, '/distinct/<string:_column>')
api.add_resource(GetDoctor, '/doctor/<int:_id>')
api.add_resource(AddDoctor, '/doctor/add')
api.add_resource(LoginDoctor, '/doctor/Login')
api.add_resource(LogoutDoctor, '/doctor/Logout')
if __name__ == '__main__':
    from db import db
    import logging

    logging.basicConfig(filename='error.log', level=logging.DEBUG)

    db.init_app(app)
    app.run(port=5000, debug=True)
